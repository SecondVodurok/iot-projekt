#!/bin/bash

HTMLOUTPUT=/opt/iotproject/index.html
DIRECTORYOLIST=/tmp

DIRECTORYOLIST=/tmp


if [$# -eq 1 ]
then
    if [ -d "$1"] ; 
    then
        DIRECTORYOLIST=$1
    else
            echo "Directory $1 not found."
            exit 1
    fi
fi
shift

echo "<html><body>" > $HTMLOUTPUT
echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT
date +%H:%M:%S >> $HTMLOUTPUT
ls $DIRECTORYOLIST >> $HTMLOUTPUT
#For loop der die existenz des files erst prueft.
for htmlsnippet in $*
do
    if [-r $htmlsnippet ]
    then
        cat $htmlsnippet >> $HTMLOUTPUT
    else
        echo "File $htmlsnippet is not readable" >&2
    fi
done


echo  "</body></html>" >> $HTMLOUTPUT