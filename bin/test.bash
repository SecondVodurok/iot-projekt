#!/bin/bash

DIRECTORYOLIST=/tmp


if [$# -eq 1 ]
then
    if [ -d "$1"] ; 
    then
        DIRECTORYOLIST=$1
    else
            echo "Directory $1 not found."
            exit 1
    fi
fi